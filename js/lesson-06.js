
const money = Number(prompt('Ваш месячный доход?', 30000));
const profit = prompt('Перечислите дополнительные работы', "фриланс");
const expenses = prompt('Перечислите возможные расходы за рассчитываемый период через запятую', "Питание, Проезд, Кино");
const amount = prompt('Во сколько обойдуться обязательные статьи расходов?', 25000);
const purpose = 30000;
const period = 3;
const deposit = confirm('Есть ли у вас вклад в банке?');



let flag = true;
let extraMoney;

while (flag) {
    extraMoney = Number(prompt(`Перечислите возможный доход за ваши дополнительные работы: ${profit}?`, 15000));

    if (typeof extraMoney === 'number') {
        if (!Number.isNaN(extraMoney)) {
            flag = false;
        }
    }
}

const accumulatedIncome = getAccumulatedIncome();
const budgetDay = accumulatedIncome / 30;




function getAccumulatedIncome() {
    return money - amount + extraMoney;
}

function getTargetMonth() {
    return purpose / accumulatedIncome;
}

console.log('Ваш бюджет на месяц с учетом ваших расходов составляет: ', getAccumulatedIncome());

const targetMonth = getTargetMonth();

if (targetMonth < 0) {
    console.log(`Ваша цель накопить ${purpose} с учетом всех ваших расходов не будет достигнута`);
} else {
    console.log(`Ваша цель накопить ${purpose} с учетом всех ваших расходов будет достигнута через`, getTargetMonth() + ' месяца');
}

console.log('Дневной бюджет', budgetDay);


// Доп задание 1
const arr = [
    "123",
    "45", 
    "67", 
    "35", 
    "26", 
    "78", 
    "567", 
];

console.log(arr);

console.log(arr.filter(function(n) {
    return n.startsWith('2') || n.startsWith('4')
}))

// Доп задание 2
function elipsis(str) {
    return str.trim().slice(0, 30) + (str.length > 30 ? '...' : '');
}

console.log(elipsis('The quick brown fox jumps over the lazy dog.'))
console.log(elipsis('Hello'))


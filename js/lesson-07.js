fetch('https://reqres.in/api/users?per_page=12')
    .then(res => res.json())
    .then(json => {
        // Идентичная запись: const data = json.data
        const { data } = json;

        divider(1);
        data.forEach(el => console.log(el.last_name));

        divider(2);
        data
            .map(el => el.last_name)
            .filter(el => el.toLowerCase().startsWith('f'))
            .forEach(el => console.log(el));

        divider(3);
        const reduceResult = data
            .map(el => `${el.first_name}  ${el.last_name}`)
            .reduce(
                (sum, el, index) => `${sum} ${el}` + (index < data.length - 1 ? ',' : ''), 
                "Наша база содержит данные следующих пользователей:"
            );
        console.log(reduceResult);

        divider(3.5);
        const startString = "Наша база содержит данные следующих пользователей:";
        const mapResult = startString + data
            .map(({first_name, last_name}) => `${first_name} ${last_name}`)
            .join(', ')
        console.log(mapResult);

        divider(4);
        const userObj = data[0];
        const userKeys = Object.keys(userObj)
        console.log(userKeys.join(', '));
    });


function divider(n) {
    console.log('-----------');
    console.log(`Пункт №${n}:`)
    console.log('-----------');
}

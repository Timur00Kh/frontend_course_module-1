function getRandomInt() {
    return Math.round(Math.random() * 10);
}

let number = getRandomInt();

let tries = 10;

function gameLoop() {
    const answer = prompt('Угадай число от 1 до 10?');
    const n = Number(answer);


    if (answer === null) {
        alert('Игра окончена');
        return;
    }

    tries--;
    if (n === number) {
        const r = confirm('Поздравляю, Вы угадали!!! Хотели бы сыграть еще?');
        if (r) {
            return restart();
        }
        return;
    } else if (tries <= 0) {
        const r = confirm('Попытки закончились, хотите сыграть еще?');
        if (r) {
            return restart();
        } else {
            return alert('Игра окончена');;
        }
    }

    if (Number.isNaN(n)) {
        alert('Введи число!');
    } else if (n > number) {
        alert(`Загаданное число меньше, осталось попыток ${tries}!`);
    } else if (n < number) {
        alert(`Загаданное число больше, осталось попыток ${tries}!`);
    }

    gameLoop();
}

function restart() {
    number = getRandomInt();
    tries = 10;
    gameLoop();
}

gameLoop();

// BOOK 5
const book5List = document.querySelector('.book:nth-child(5) ul');
const items = book5List.querySelectorAll('li');

book5List.innerHTML = '';
book5List.append(
    items[0],
    items[1], 
    items[9],
    items[3],
    items[4],
    items[2],
    items[6],
    items[7],
    items[5],
    items[8],
    items[10]
);


// BOOK 6
const book6LastItem = document.querySelector('.book:nth-child(6) ul li:last-child');
const chapter = document.createElement('li');
chapter.innerText = 'Глава 8: Запределами ES6';
book6LastItem.before(chapter);


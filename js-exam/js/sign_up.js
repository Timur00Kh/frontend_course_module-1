'use strict';

const signUp = document.getElementById('sign-up');

const emailFieldset = document.getElementById('email-fieldset');
const passwordFieldset = document.getElementById('password-fieldset');
const checkboxFieldset = document.getElementById('checkbox-fieldset');

const requiredError = 'Поле обязательно для заполнения';
const fieldsetErrorClass = 'fieldset_error';

signUp.addEventListener('submit', (event) => {
    event.preventDefault();

    const emailIsValid = validateFieldset(emailFieldset, getEmailError);
    const passwordIsValid = validateFieldset(passwordFieldset, getPasswordError);
    const checkboxIsValid = validateFieldset(checkboxFieldset, getCheckboxError);

    const formIsValid = emailIsValid && passwordIsValid && checkboxIsValid;

    if (formIsValid) {
        localStorage.setItem('auth', JSON.stringify({email, password}));
        console.log('Успешная авторизация!');
        window.location.replace('./sign_in.html')
    }
});

function validateFieldset(fieldset, validate) {
    const input = fieldset.querySelector('input');
    const error = fieldset.querySelector('.fieldset__error');

    const value = input.type === 'checkbox' ? input.checked : input.value;
    const errorText = validate(value);
    if (errorText) {
        fieldset.classList.add(fieldsetErrorClass);
        error.innerText = errorText;
        return false;
    } else {
        fieldset.classList.remove(fieldsetErrorClass);
    }
    return true;
}

function getPasswordError(password) {
    if (password.length <= 0) {
        return requiredError;
    }
    if (password.length < 8) {
        return 'Пароль должен содержать как минимум 8 символов';
    }
    return '';
}

function getEmailError(email) {
    if (email.length <= 0) {
        return requiredError;
    }

    const re =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = re.test(String(email).toLowerCase());
    
    if (!isValid) {
        return 'Email невалидный';
    }
    return '';
}

function getCheckboxError(value) {
    if (!value) {
        return requiredError;
    }
    return '';
}

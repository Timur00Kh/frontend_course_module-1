

const signUp = document.getElementById('sign-up');
const signUpEmail = document.getElementById('sign-up-email');
const signUpPassword = document.getElementById('sign-up-password');
const signUpChechbox = document.getElementById('checkbox');
const passwordError = document.getElementById('password-error');
const emailError = document.getElementById('email-error');
const checkboxError = document.getElementById('checkbox-error');

const requiredError = 'Поле обязательно для заполнения';
const errorInputClass = 'fieldset__input_error';
const fildsetErrorClass = 'fieldset__label_error';

signUp.addEventListener('submit', (event) => {
    event.preventDefault();
    const email = signUpEmail.value;
    const password = signUpPassword.value;
    const checkbox = signUpChechbox.checked;

    let formIsValid = true;

    try {
        emailError.style.display = 'none';
        signUpEmail.classList.remove(errorInputClass);
        validateEmail(email);
    } catch ({message}) {
        console.error(message);
        emailError.innerText = message;
        emailError.style.display = 'block';
        signUpEmail.classList.add(errorInputClass);
        formIsValid = false;
    }

    try {
        passwordError.style.display = 'none';
        signUpPassword.classList.remove(errorInputClass);
        validatePassword(password);
    } catch ({message}) {
        console.error(message);
        passwordError.innerText = message;
        passwordError.style.display = 'block';
        signUpPassword.classList.add(errorInputClass);
        formIsValid = false;
    }

    if (!checkbox) {
        checkboxError.style.display = 'block';
        formIsValid = false;
        console.error(requiredError);
        signUpChechbox.classList.remove(errorInputClass);
    } else {
        checkboxError.style.display = 'none';
        signUpChechbox.classList.add(errorInputClass);
    }


    if (formIsValid) {
        localStorage.setItem('auth', JSON.stringify({email, password}));
        console.log('Успешная авторизация!');
        window.location.replace('./sign_in.html')
    }
});

function validatePassword(password) {
    if (password.length <= 0) {
        throw new Error(requiredError);
    }
    if (password.length < 8) {
        throw new Error('Пароль должен содержать как минимум 8 символов');
    }
}

function validateEmail(email) {
    if (email.length <= 0) {
        throw new Error(requiredError);
    }

    const re =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = re.test(String(email).toLowerCase());
    
    if (!isValid) {
        throw new Error('Email невалидный');
    }
}

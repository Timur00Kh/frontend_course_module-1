
const header = document.getElementById('user');
const logOutBtn = document.getElementById('log_out');


function showUser() {
    const userData = localStorage.getItem('auth');
    const user = JSON.parse(userData);

    if (user) {
        header.innerText = user.email;
    } else {
        header.innerText = 'Вы не авторизованы';
        logOutBtn.innerText = 'Авторизоваться'
    }
}

logOutBtn.addEventListener('click', () => {
    const userData = localStorage.getItem('auth');
    const user = JSON.parse(userData);

    if (user) {
        localStorage.removeItem('auth');
    }

    window.location.replace('./sign_in.html');
});

showUser()
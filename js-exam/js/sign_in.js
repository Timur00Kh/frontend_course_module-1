const signUp = document.getElementById('sign-up')
const signUpEmail = document.getElementById('sign-up-email')
const signUpPassword = document.getElementById('sign-up-password')
const signUpChechbox = document.getElementById('checkbox')
const passwordError = document.getElementById('password-error')
const emailError = document.getElementById('email-error')
const checkboxError = document.getElementById('checkbox-error')

const requiredError = 'Поле обязательно для заполнения'
const errorInputClass = 'fieldset__input_error'
const fildsetErrorClass = 'fieldset__label_error'

signUp.addEventListener('submit', (event) => {
    event.preventDefault()
    const email = signUpEmail.value
    const password = signUpPassword.value

    const userData = localStorage.getItem('auth')
    const user = JSON.parse(userData)

    if (!user) {
        showWrongDataError()
        console.error('Данных о пользователях нет!')
        return
    }

    if (email === user.email && password === user.password) {
        window.location.replace('./user.html')
    } else {
        showWrongDataError()
    }
})

function showWrongDataError() {
    const message = 'Не верные email или пароль!'
    passwordError.innerText = message
    passwordError.style.display = 'block'
    signUpPassword.classList.add(errorInputClass)
    emailError.innerText = message
    emailError.style.display = 'block'
    signUpEmail.classList.add(errorInputClass)
}

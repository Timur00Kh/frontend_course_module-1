
const activeClass = 'burger-menu_active';
const burgerActiveClass = 'burger_active';
let menuAcive = false;


function callback() {
    const burgerMenu = document.getElementById('burger-menu');
    const burger = document.getElementById('burger');
    if (menuAcive) {
        burgerMenu.classList.remove(activeClass);
        burger.classList.remove(burgerActiveClass);
        menuAcive = false;
        document.body.style.overflow = '';
    } else {
        burgerMenu.classList.add(activeClass);
        burger.classList.add(burgerActiveClass);
        document.body.style.overflow = 'hidden';
        menuAcive = true
    }
}

document.getElementById('burger').addEventListener('click', callback);

const modalBtn = document.getElementById('modal-btn');
const modal = document.getElementById('modal');
const modalBackdrop = document.getElementById('modal-backdrop');
const modalClose = document.getElementById('modal-close');

const activeBackdropClass = 'modal-backdrop_active';
const activeModalWrapperClass = 'modal-wrapper_active';
let modalActive = false;

function toggleModal() {
    if (modalActive) {
        modal.classList.remove(activeModalWrapperClass);
        modalBackdrop.classList.remove(activeBackdropClass);
    } else {
        modal.classList.add(activeModalWrapperClass);
        modalBackdrop.classList.add(activeBackdropClass);
    }
    modalActive = !modalActive;
}

modalBtn.addEventListener('click', () => {
    toggleModal();
});

modalClose.addEventListener('click', () => {
    toggleModal();
});

modal.addEventListener('click', (event) => {
    if (event.target === event.currentTarget) {
        toggleModal();
    }
});







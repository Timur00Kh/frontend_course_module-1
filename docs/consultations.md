# Записи консультаций

Если вы смотрите повторно консультации, просьба прислать краткое описание консультации наставнику в телеграмм (@Timur00Kh)

## Июнь

-   [20.06.2022](https://events.webinar.ru/55331401/11676453/record-new/12078929)
-   [23.06.2022](https://events.webinar.ru/55331401/11713705/record-new/12120165)
-   [27.06.2022](https://events.webinar.ru/55331401/11735263/record-new/12144751)
-   [30.06.2022](https://events.webinar.ru/55331401/11713705/record-new/12120493)

## Июль

-   [04.07.2022](https://events.webinar.ru/55331401/11735263/record-new/12145191)
-   [07.07.2022](https://events.webinar.ru/55331401/11713705/record-new/12181453)
-   [11.07.2022](https://events.webinar.ru/55331401/11713705/record-new/12233881)
-   [14.07.2022](https://events.webinar.ru/55331401/11713705/record-new/12255825)
-   [18.07.2022](https://events.webinar.ru/55331401/11713705/record-new/12287399)
-   [21.07.2022](https://events.webinar.ru/55331401/11713705/record-new/12308701)
-   [25.07.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12335439)
-   [28.07.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12359693)


## Август
- [03.08.2022](https://events.webinar.ru/innopolisooc/12060485/record-new/12497361) - сверстали адаптивный хедер и мейн по БЭМу. А также заменили картинки с тенями в [сервисах](../images/services/2.jpg) и [портфолио](../images/portfolio/2.jpg) `hw-14`
- [04.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12479243) - переключились с css на scss, сверстали адаптив для блоков “Наши услуги” / “О компании” / “Портфолио”.
- [08.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12509443) - Наставник отвечал на вопросы по верстке и гиту всю консультацию
- [11.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12530979)
- [18.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12589409)
- [22.08.22](https://events.webinar.ru/innopolisooc/11713705/record-new/12623937) - Домашка 4 по JS, верстали блок вопрос-ответ с анимациями. 
- [25.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12649357) - Делали пятую домашку по JS с рекурсиями. Реализовали бургер меню на JS и SCSS
- [29.08.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/12689775) - Делали шестую домашку по JS. Реализовали модальное окно на JS и SCSS

## Сентябрь
- [01.09.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/144652166) - Делали шестую домашку по JS. Запросы к серверу и обработку результатов запроса. Использовали много методов массивов. Рассказал не много про API. Разобрали домашки студентов.
- [05.09.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/1655558699) - Верстали форму и писали обработку формы из Промежуточной по JS
- [08.09.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/1607387315) - Сверстали страницу регистрации, авторизации и пользователя из Промежуточной по JS
- [12.09.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/950108601) - Сделали доп задание из 9 урока. Переделали валидвцию формы в промежуточной по JS.
- [15.09.2022](https://events.webinar.ru/innopolisooc/11713705/record-new/1826063611) - Инициализировали Реакт проект. Рассмотрели [RoadMap](https://roadmap.sh/frontend) Фронтенд разработчика и алалоги реакта. Сверстали форму на реакте с валидацией в реальном времени. 
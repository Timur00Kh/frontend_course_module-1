# Репозиторий для консультаций

### Тексты всех домашек по JS

-   [homeworks-js.md](./docs/homeworks-js.md)

### Тексты всех домашек

-   [homeworks.md](./docs/homeworks.md)

### Записи консультаций

-   [consultations.md](./docs/consultations.md)

### Посмотреть последнюю опубликованную версию ДЗ с консультации:

- https://timur00kh.gitlab.io/frontend_course_module-1/
- https://timur00kh.gitlab.io/frontend_course_module-1/js-exam/user

## Репозиторий по реакту
- https://gitlab.com/Timur00Kh/module-react
